<?php
/**
 * @file
 * podcast.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function podcast_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'podcast';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'podcast';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'podcast';
  $handler->display->display_options['link_display'] = 'page';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['class'] = 'podcast';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = 0;
  $handler->display->display_options['row_options']['comments'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['view_node']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['view_node']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['view_node']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['view_node']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['view_node']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['view_node']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['view_node']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['view_node']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['view_node']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['view_node']['alter']['html'] = 0;
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['view_node']['hide_empty'] = 0;
  $handler->display->display_options['fields']['view_node']['empty_zero'] = 0;
  $handler->display->display_options['fields']['view_node']['hide_alter_empty'] = 1;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['created']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  /* Field: Content: Audio */
  $handler->display->display_options['fields']['field_episode_audio']['id'] = 'field_episode_audio';
  $handler->display->display_options['fields']['field_episode_audio']['table'] = 'field_data_field_episode_audio';
  $handler->display->display_options['fields']['field_episode_audio']['field'] = 'field_episode_audio';
  $handler->display->display_options['fields']['field_episode_audio']['label'] = '';
  $handler->display->display_options['fields']['field_episode_audio']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_episode_audio']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_episode_audio']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_episode_audio']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_episode_audio']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_episode_audio']['field_api_classes'] = 0;
  /* Field: Content: Subtitle */
  $handler->display->display_options['fields']['field_episode_subtitle']['id'] = 'field_episode_subtitle';
  $handler->display->display_options['fields']['field_episode_subtitle']['table'] = 'field_data_field_episode_subtitle';
  $handler->display->display_options['fields']['field_episode_subtitle']['field'] = 'field_episode_subtitle';
  $handler->display->display_options['fields']['field_episode_subtitle']['label'] = '';
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['text'] = '<itunes:subtitle>[field_episode_subtitle-value]</itunes:subtitle>';
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_episode_subtitle']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['empty'] = 'empty!';
  $handler->display->display_options['fields']['field_episode_subtitle']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_episode_subtitle']['field_api_classes'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'episode' => 'episode',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'podcast';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'podcast';
  $handler->display->display_options['menu']['name'] = 'main-menu';

  /* Display: Feed */
  $handler = $view->new_display('feed', 'Feed', 'feed');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['style_options']['description'] = 'Description as input in the view';
  $handler->display->display_options['row_plugin'] = 'node_rss';
  $handler->display->display_options['row_options']['links'] = 0;
  $handler->display->display_options['path'] = 'podcast.xml';
  $handler->display->display_options['displays'] = array(
    'default' => 'default',
    'page' => 'page',
  );
  $export['podcast'] = $view;

  return $export;
}
