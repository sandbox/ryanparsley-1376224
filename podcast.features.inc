<?php
/**
 * @file
 * podcast.features.inc
 */

/**
 * Implements hook_views_api().
 */
function podcast_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function podcast_node_info() {
  $items = array(
    'episode' => array(
      'name' => t('Podcast Episode'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
